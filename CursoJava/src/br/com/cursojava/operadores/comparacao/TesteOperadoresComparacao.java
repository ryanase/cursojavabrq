package br.com.cursojava.operadores.comparacao;

import br.com.cursojava.operadores.comparacao.apoio.TesteReferencia;


public class TesteOperadoresComparacao {

	public static void main(String[] args) {
		// Teste de operadores de compara��o
		// igual
		boolean testeIgual = 2 == 3;
		// diferente
		boolean testeDiferente = 2 != 3;
		// menor
		boolean testeMenor = 2 < 3;
		// menor igual
		boolean testeMenorIgual = 2 <= 2;
		// maior
		boolean testeMaior = 2 > 3;
		// maior igual
		boolean testeMaiorIgual = 2 >= 2;

		System.out.println(testeIgual);
		System.out.println(testeDiferente);
		System.out.println(testeMenor);
		System.out.println(testeMenorIgual);
		System.out.println(testeMaior);
		System.out.println(testeMaiorIgual);

		// Operadores de igual (==) e diferente (!=) tamb�m s�o aplic�veis
		// a objetos, por�m comparam suas referencias e n�o valores
		TesteReferencia diferente = new TesteReferencia();
		diferente.conteudo = "1";
		TesteReferencia igual = new TesteReferencia();
		igual.conteudo = "1";
		System.out.println(diferente);
		System.out.println(igual);

		boolean testeIgualObjetos = diferente == igual;
		System.out.println(testeIgualObjetos);
		boolean testeDiferenteObjetos = diferente != igual;
		System.out.println(testeDiferenteObjetos);
	}
}