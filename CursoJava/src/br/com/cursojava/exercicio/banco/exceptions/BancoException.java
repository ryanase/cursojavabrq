package br.com.cursojava.exercicio.banco.exceptions;

public class BancoException extends Exception {

	public BancoException() {
		super();
	}

	public BancoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BancoException(String message, Throwable cause) {
		super(message, cause);
	}

	public BancoException(String message) {
		super(message);
	}

	public BancoException(Throwable cause) {
		super(cause);
	}
	
}