package br.com.cursojava.exercicio.banco.exceptions;

public class ClienteNaoEncontradoException extends BancoException {

	public ClienteNaoEncontradoException() {
		super();
	}

	public ClienteNaoEncontradoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ClienteNaoEncontradoException(String message, Throwable cause) {
		super(message, cause);
	}

	public ClienteNaoEncontradoException(String message) {
		super(message);
	}

	public ClienteNaoEncontradoException(Throwable cause) {
		super(cause);
	}

}