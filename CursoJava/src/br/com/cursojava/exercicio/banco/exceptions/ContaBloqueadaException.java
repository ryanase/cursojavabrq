package br.com.cursojava.exercicio.banco.exceptions;

public class ContaBloqueadaException extends BancoException {

	public ContaBloqueadaException() {
		super();
	}

	public ContaBloqueadaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ContaBloqueadaException(String message, Throwable cause) {
		super(message, cause);
	}

	public ContaBloqueadaException(String message) {
		super(message);
	}

	public ContaBloqueadaException(Throwable cause) {
		super(cause);
	}

}