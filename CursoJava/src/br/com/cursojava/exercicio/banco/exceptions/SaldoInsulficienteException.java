package br.com.cursojava.exercicio.banco.exceptions;

public class SaldoInsulficienteException extends BancoException {

	public SaldoInsulficienteException() {
		super();
	}

	public SaldoInsulficienteException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SaldoInsulficienteException(String message, Throwable cause) {
		super(message, cause);
	}

	public SaldoInsulficienteException(String message) {
		super(message);
	}

	public SaldoInsulficienteException(Throwable cause) {
		super(cause);
	}

}